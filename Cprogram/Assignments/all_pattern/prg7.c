/*
 				*
			*	*	*
		*	*	*	*	*
	*	*	*	*	*	*	*
*/
#include<stdio.h>
 void main() {
	 int row;
	 printf("Enter number of rows: ");
	 scanf("%d", &row);
	 for (int i = 0 ; i < row; i++) {
		 for (int j = 0; j < (row-i)-1; j++) {
			 printf("	");
		 }
		 for (int k = 0; k < (i*2)+1; k++) {
			 printf("*	");
		 }
		 printf("\n");
	 }
 }

