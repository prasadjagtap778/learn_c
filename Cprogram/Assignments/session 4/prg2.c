/*
 1	2	3	4
 1	2	3
 1	2
 1
 */
#include<stdio.h>
 void main() {
	 int row;
	 printf("Enter a number of rows: ");
	 scanf("%d",&row);
	 for (int i = 0; i < row; i ++) {
		 int num = 1;
		 for (int j = 0; j < (row-i); j++) {
			 printf("%d  ", num);
			 num++;
		 }
		 printf("\n");
	 }

	}
