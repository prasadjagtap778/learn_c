/*
 0	0	0	0	0
 	1	2	3	4
		4	6	8
			9	12
				16
*/
#include<stdio.h>
 void main() {
	 int num = 0, row;
	 printf("Enter number of rows: ");
	 scanf("%d", &row);
	 for (int i = 0; i < row; i++) {
		 for (int j = 0; j < i ; j++) {
			 printf("	");
		 }
		 int num1 = num;
		 for (int k = 0; k < (row-i); k++) {
			 printf("%d	",num1);
			 num1 = num1 + i;
		 }
		 num = (i*2)+1+num;
		 printf("\n");
	 }
 }

