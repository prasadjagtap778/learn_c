/*
 				D
			C	D	C
		B	C	D	C	B
	A	B	C	D	C	B	A
*/

#include<stdio.h>
 void main() {
	 int row;
	 printf("Enter number of rows: ");
	 scanf("%d", &row);

	 for (int i = 0; i < row; i++) {
		 int num = (64 + row) - i;
		 for (int j = 0; j < (row-i)-1; j++) {
			 printf("	");
		 }
		 for (int k = 0; k < (i*2) + 1; k++) {
			 printf("%c	", num);
			 num = num + 1;
		 }
		 printf("\n");
	 }
 }

