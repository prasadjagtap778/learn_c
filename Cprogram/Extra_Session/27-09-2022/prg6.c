/*
WAP to print factorial of number of odd digits in a given number 
Input : 530221
Output : factorial of 5 is 120
	 factorial of 3 is 6
	 factorial of 1 is 1
*/
#include<stdio.h>
 void main() {
	 int num,digi; 
	 printf("Enter number: ");
	 scanf("%d", &num);
	 int temp = num;
	 while (temp != 0) {
		 digi = temp % 10;
		 if (digi % 2 == 1) {
			 int fact = 1;
			 for (int i = 1; i <= digi; i++) {
				 fact = fact * i;
			 }
			 printf("Factorial of %d is %d",digi,fact);
			 temp = temp / 10;
		 }
	 }
 }

