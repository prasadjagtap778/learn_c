// 3) WAP to print all even numbers in reverse order and odd numbers in the standard way. Both separately. Within a range.
#include<stdio.h>
 void main() {
	 int a,b;
	 printf("Enter a start number: ");
	 scanf("%d", &a);
	 printf("Enter a end number: ");
	 scanf("%d", &b);
	 printf("The even number is: ");
	 for (int c = b; c >= a; c--) {
		 if (c%2 == 0) {
			 printf("%d ", c);
		 }
	 }
	 printf("\n");
	 printf("The odd numbers is: ");
	 for (int d = a; d <= b; d++) {
		 if (d%2 == 1) {
			 printf("%d ", d);

		 }
	 }
	 printf("\n");
 }

