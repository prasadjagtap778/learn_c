// 5) WAP to take the Number input and print all the factors of that number.

#include<stdio.h>
 void main() {
	 int a;
	 printf("Enter a number: ");
	 scanf("%d", &a);
	 printf("\n");
	 printf("The factors of number are: ");
	 for (int i = 1; i <= a; i++) {
		 if (a%i==0) {
			 printf("%d ", i);
		 }
	 }
	 printf("\n");
 }
