// WAP to print the numbers in a given range and their multiplicative inverse.
// Suppose x is a number then its multiplicative inverse or reciprocal is 1/x.
// The expected output for range 1 - 5
// 1 = 1
// 2 = 1/2 i.e 0.5
// 3 = 1/3 i.e 0.33
// 4 = 0.25
// 5 = 0.20

#include<stdio.h>
 void main() {
	 double a, b;
	 printf("Enter a start number: ");
	 scanf("%lf", &a);
	 printf("Enter a end number: ");
	 scanf("%lf", &b);

	 for (double i = a; i <= b; i++) {
		 printf("The multiplicative inverse of number %lf is %lf\n", i, 1/i);
	 }
 }

