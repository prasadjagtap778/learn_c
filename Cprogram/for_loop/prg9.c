// 9) WAP to calculate the square root of a number ranging from 100 to 300

#include<stdio.h>
 void main() {
	 int a,b;
	 printf("Enter a start number: ");
	 scanf("%d", &a);
	 printf("Enter a end number: ");
	 scanf("%d", &b);

	 for (int i = a; i <= b; i++) {
		 printf("The square root of %d is %d\n", i, i*i);
	 }
 }

