// 7) WAP to calculate the LCM of given two numbers.

#include<stdio.h>
 int main() {
    int a,b,rem;
    printf("Enter a two positive numbers: ") ;
    scanf("%d %d", &a ,&b);
    rem = (a > b)? a : b;
    while(1) {

    		if (rem % a == 0 && rem % b == 0) {
	    		printf("The LCM of number %d and %d is %d\n", a, b, rem);
			break;
    		}

    		++rem;
    }

    
    return 0;
   
 }

