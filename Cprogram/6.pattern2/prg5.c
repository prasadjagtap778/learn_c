/*
	D	C	B	A
	e	d	c	b
	F	E	D	C
	g	f	e	d
*/

#include<stdio.h>
 void main() {
	 int row,col,temp;
	 printf("Enter a number of rows: ");
	 scanf("%d", &row);
	 printf("Enter a number of cols: ");
	 scanf("%d", &col);
	// temp = row;
	 for (int i = 0; i < row; i++) {
		 temp = row + i;	
		 for (int j = 0; j < col; j++) {
			 if (i%2 == 0) {
				 printf(" %c",64 + temp);
				 temp--;
			 }else{
				 printf(" %c", 96 + temp);
				 temp--;
			 }
		 }
		 printf("\n");
	 }
 }


