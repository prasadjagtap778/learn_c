/*
  1	2	3	4
  25	36	49	64
  9	10	11	12
  169	196	225	256
*/
#include<stdio.h> 
 void main() {
	 int row,col,num = 1;
	 printf("Enter number of rows: ");
	 scanf("%d", &row);
	 printf("Enter number of cols: ");
	 scanf("%d", &col);

	 for (int i = 1; i <= row; i++) {
		 for (int j = 1; j <= col; j++) {
			 if (i%2 == 1) {
				 printf("%d	", num);
				 num++;
			 }else{
				 printf("%d	", num*num );
				 num++;
			 }
		 }
		 printf("\n");
	 }
 }

