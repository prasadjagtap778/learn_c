/*
	I	H	G
	F	E	D
	C	B	A
*/
#include<stdio.h>
 void main() {
	 int row,col,temp,num;
	 printf("Enter a number of rows: ");
	 scanf("%d", &row);
	 printf("Enter a number of cols: ");
	 scanf("%d", &col);
	 temp = row * row;
	 
	 for (int i = 1; i <= row; i++) {
		for (int j = 1; j <= col; j++)  {
			printf(" %c", temp+64);
			temp--;
		}
		printf("\n");
		}
 }

